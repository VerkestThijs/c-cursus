create database Abonnees;
go
use Abonnees;
go
create table Abonnees (
  AbonneeNr int identity primary key,
  Voornaam nvarchar(50),
  Familienaam nvarchar(50),
  EmailAdres nvarchar(50)
);
go
create unique index AbonneesEmailAdres on Abonnees(EmailAdres);
go
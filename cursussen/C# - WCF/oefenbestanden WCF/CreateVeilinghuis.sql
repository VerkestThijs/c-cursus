create database Veilinghuis;
go
use Veilinghuis;
go
create table Veilingen (
  VeilingNr int identity primary key,
  Kunstwerk nvarchar(50),
  EmailAdresHoogsteBieder nvarchar(50),
  HoogsteBod decimal(10,2) not null default 0 
);
go
insert into Veilingen(Kunstwerk) values ('De paardebloemen')
insert into Veilingen(Kunstwerk) values ('De meeuw')
insert into Veilingen(Kunstwerk) values ('Mina Losa')
create database BankWCF;
go
use BankWCF;
go
create table Rekeningen (
  RekeningNr nvarchar(20) primary key,
  Saldo decimal(10,2) not null default 0
);
go

insert into Rekeningen(RekeningNr,Saldo) values ('123-4567890-02',1000);
insert into Rekeningen(RekeningNr,Saldo) values ('234-5678901-69',2000);
insert into Rekeningen(RekeningNr,Saldo) values ('345-6789012-12',500);
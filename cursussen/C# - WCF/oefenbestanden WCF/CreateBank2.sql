create database BankWCF2;
go
use BankWCF2;
go
create table Rekeningen (
  RekeningNr nvarchar(20) primary key,
  Saldo decimal(10,2) not null default 0
);
go

insert into Rekeningen(RekeningNr,Saldo) values ('456-7890123-73',4000);
insert into Rekeningen(RekeningNr,Saldo) values ('567-8901234-53',5000);
insert into Rekeningen(RekeningNr,Saldo) values ('678-9012345-96',6000);
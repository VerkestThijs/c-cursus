create database Magazijnen;
go
use Magazijnen;
go
create table Magazijnen (
  MagazijnNr int not null,
  ArtikelNr int not null,
  Voorraad int not null
);
go
alter table Magazijnen add constraint MagazijnenMagazijnNrArtikelNr primary key clustered (MagazijnNr,Artikelnr);

insert into Magazijnen(MagazijnNr,ArtikelNr,Voorraad) values (1,10,1000);
insert into Magazijnen(MagazijnNr,ArtikelNr,Voorraad) values (1,20,2000);
insert into Magazijnen(MagazijnNr,ArtikelNr,Voorraad) values (2,30,3000);
insert into Magazijnen(MagazijnNr,ArtikelNr,Voorraad) values (4,40,4000);
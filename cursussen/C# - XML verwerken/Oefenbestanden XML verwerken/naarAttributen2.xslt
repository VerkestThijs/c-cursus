<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:prs="http://www.vdab.be/opleidingen/personeel">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/prs:personeel">
    <xsl:element name="personeel">
      <xsl:apply-templates select="prs:afdeling">
        <xsl:sort select="prs:naam"/>
      </xsl:apply-templates>
    </xsl:element>
  </xsl:template>
  <xsl:template match="prs:afdeling">
    <xsl:element name="afdeling">
      <xsl:attribute name="naam">
        <xsl:value-of select="prs:naam"/>
      </xsl:attribute>
      <xsl:apply-templates select="prs:werknemer">
        <xsl:sort select="prs:naam"/>
      </xsl:apply-templates>
    </xsl:element>
  </xsl:template>
  <xsl:template match="prs:werknemer">
    <xsl:element name="werknemer">
      <xsl:attribute name="nr">
        <xsl:value-of select="@nr"/>
      </xsl:attribute>
      <xsl:value-of select="prs:naam"/>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>

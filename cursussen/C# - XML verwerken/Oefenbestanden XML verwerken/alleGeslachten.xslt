<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:prs="http://www.vdab.be/opleidingen/personeel"
                exclude-result-prefixes="prs">
  <xsl:output method="html" doctype-public="-//W3C//DTD HTML 4.01//EN" doctype-system="http://www.w3.org/TR/html4/strict.dtd"/>
  <xsl:template match="/prs:personeel">
    <html>
      <head>
        <title>Alle geslachten</title>
      </head>
      <body>
        <h1>Personeel</h1>
        <xsl:apply-templates select="prs:afdeling"/>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="prs:afdeling">
    <h2>
      <xsl:value-of select="prs:naam"/>
    </h2>
    <xsl:apply-templates select="prs:werknemer"/>
  </xsl:template>
  <xsl:template match="prs:werknemer">
    <p>
      <xsl:choose>
        <xsl:when test="prs:geslacht='V'">
          <img src="vrouw.gif" alt="vrouw"/>
        </xsl:when>
        <xsl:otherwise>
          <img src="man.gif" alt="man"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:value-of select="prs:naam"/>
    </p>
  </xsl:template>
</xsl:stylesheet>

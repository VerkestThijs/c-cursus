<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" doctype-public="-//W3C//DTD HTML 4.01//EN" doctype-system="http://www.w3.org/TR/html4/strict.dtd"/>
  <xsl:template match="/personeel">
    <html>
      <head>
        <title>Feest</title>
      </head>
      <body>
        <h1>Relatiegeschenken</h1>
        <table border="1">
          <tr>
            <th>Naam</th>
            <th>Dienstjaren</th>
            <th>Geschenk</th>
          </tr>
          <xsl:apply-templates select="werknemer"/>
        </table>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="werknemer">
    <tr>
      <td>
        <xsl:value-of select="naam"/>
      </td>
      <td style="text-align:right">
        <xsl:value-of select="jaren"/>
      </td>
      <td>
        <xsl:choose>
          <xsl:when test="jaren&gt;=30">
            <img src="champagne.gif"
            alt="champagne"/>
          </xsl:when>
          <xsl:otherwise>
            <img src="ticket.gif"
            alt="ticket"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>

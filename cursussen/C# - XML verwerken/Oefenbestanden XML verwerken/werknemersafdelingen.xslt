<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes"/>
  <xsl:template match="/personeel">
    <table border="1">
      <tr>
        <th>Naam</th>
        <th>Geslacht</th>
        <th>Afdeling</th>
      </tr>
      <xsl:apply-templates select="werknemer"/>
    </table>
  </xsl:template>
  <xsl:template match="werknemer">
    <tr>
      <td>
        <xsl:value-of select="naam"/>
      </td>
      <td>
        <xsl:choose>
          <xsl:when test="geslacht='V'">
            <img src="/Content/Images/vrouw.gif" alt="vrouw"/>
          </xsl:when>
          <xsl:otherwise>
            <img src="/Content/Images/man.gif" alt="man"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
      <td>
        <xsl:value-of select="afdeling/naam"/>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>


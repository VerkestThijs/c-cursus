<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:prs="http://www.vdab.be/opleidingen/personeel" exclude-result-prefixes="prs">
   <xsl:output method="html"/>
   <xsl:param name="minWedde"/>
   <xsl:template match="/prs:personeel">
     <table border="1">
       <tr>
         <th>Naam</th>
         <th>Wedde</th>
       </tr>
       <xsl:apply-templates select="prs:afdeling/prs:werknemer[prs:wedde&gt;=$minWedde]">
	     <xsl:sort select="prs:wedde" data-type="number"/>
       </xsl:apply-templates>
      </table>
    </xsl:template>
    <xsl:template match="prs:werknemer">
      <tr>
        <td><xsl:value-of select="prs:naam"/></td> 
        <td><xsl:value-of select="prs:wedde"/></td>
      </tr>
    </xsl:template>
</xsl:stylesheet>
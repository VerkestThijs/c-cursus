<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:prs="http://www.vdab.be/opleidingen/personeel">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/prs:personeel">
    <xsl:element name="personeel">
      <xsl:apply-templates select="prs:afdeling/prs:werknemer">
        <xsl:sort select="prs:naam"/>
      </xsl:apply-templates>
    </xsl:element>
  </xsl:template>
  <xsl:template match="prs:werknemer">
    <xsl:element name="werknemer">
      <xsl:attribute name="nr">
        <xsl:value-of select="@nr"/>
      </xsl:attribute>
      <xsl:attribute name="naam">
        <xsl:value-of select="prs:naam"/>
      </xsl:attribute>
      <xsl:attribute name="geslacht">
        <xsl:value-of select="prs:geslacht"/>
      </xsl:attribute>
      <xsl:attribute name="indienst">
        <xsl:value-of select="prs:indienst"/>
      </xsl:attribute>
      <xsl:attribute name="wedde">
        <xsl:value-of select="prs:wedde"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>

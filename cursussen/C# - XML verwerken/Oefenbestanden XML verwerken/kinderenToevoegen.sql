use personeel
go
alter table werknemer add kinderen xml
go
update werknemer set kinderen='<kinderen><kind naam="Albert"><kind naam="Filip"/><kind naam="Laurent"/></kind></kinderen>' where nr=1
update werknemer set kinderen='<kinderen><kind naam="Jip"/><kind naam="Janneke"/></kinderen>' where nr=2
update werknemer set kinderen='<kinderen><kind naam="Robinson"/></kinderen>' where nr=3
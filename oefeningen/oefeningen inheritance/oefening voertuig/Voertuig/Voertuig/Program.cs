﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voertuig
{
    class Program
    {
        static void Main(string[] args)
        {
            Vrachtwagen mijnvrachtwagen = new Vrachtwagen("Ja", 40000m, 500, 30, "1-ABC-123", 10000);
            mijnvrachtwagen.Afbeelden();
            mijnvrachtwagen.LijnTekenaar('-', 25);
            Personenwagen pw = new Personenwagen("Piet", 15000m, 8, 6.5f, "1-DEF-4156", 5, 5);
            pw.Afbeelden();
            pw.LijnTekenaar('-', 25);
            Personenwagen pw2 = new Personenwagen();
            pw2.Afbeelden();
        }
    }
}

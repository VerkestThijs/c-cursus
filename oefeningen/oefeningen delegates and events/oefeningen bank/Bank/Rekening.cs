﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public abstract class Rekening :ISpaarmiddel
    {
        public delegate void transactie(Rekening rekening);

        public event transactie RekeningUitreksel;
        public event transactie SaldoInHetRood;
        private readonly DateTime eersteCreatie = new DateTime(1900, 1, 1);
        private string nummervalue;

        public string Nummer
        {
            get { return nummervalue; }
            set
            {
                if (RekeningControleur(value))
                {
                    nummervalue = value;
                }
            }
        }





        private decimal vorigSaldoValue;

        public decimal VorigSaldo
        {
            get { return vorigSaldoValue; }
            set { vorigSaldoValue = value; }
        }


        private decimal saldoValue;

        public decimal Saldo
        {
            get { return saldoValue; }
            set { saldoValue = value; }
        }

        private DateTime creatieDatumValue;

        public DateTime CreatieDatum
        {
            get { return creatieDatumValue; }
            set
            {
                if (value >= eersteCreatie)
                {
                    creatieDatumValue = value;
                }
            }
        }

        private bool RekeningControleur(string rekeningnr)
        {
            if (rekeningnr.Substring(0, 2) != "BE")
            {
                return false;
            }
            int derdeenvierde;
            if (!int.TryParse(rekeningnr.Substring(2, 2), out derdeenvierde))
            {
                return false;
            }

            if (!ulong.TryParse(rekeningnr.Substring(4, 12), out ulong belgischRekeningNummer))
            {
                return false;
            }

            ulong eerstetien = belgischRekeningNummer / 100ul;
            int laatste2 = (int)(belgischRekeningNummer % 100ul);
            return (int)(eerstetien % 97ul) == laatste2;

        }

        public void Afhalen (decimal bedrag)
        {
            VorigSaldo = Saldo;
            if (bedrag <= Saldo)
            {
                Saldo -= bedrag;
                if (RekeningUitreksel != null)
                {
                    RekeningUitreksel(this);
                }
                
            }
            else
            {
                if (SaldoInHetRood != null)
                {
                    SaldoInHetRood(this);
                }
            }

            
        }

        public void Storten(decimal bedrag)
        {
            VorigSaldo = Saldo;
            Saldo += bedrag;
            if (RekeningUitreksel != null)
            {
                RekeningUitreksel(this);
            }
        }

        public virtual void Afbeelden()
        {
            Console.WriteLine($"rekeningnummer: {Nummer}");
            Console.WriteLine($"saldo: {Saldo} euro");
            Console.WriteLine($"creatiedatum: {CreatieDatum:dd-MM-yyyy}");
            if (Eigenaar != null)
            {
                Console.WriteLine($"eigenaar:{Eigenaar.ToString()} ");
                Eigenaar.Afbeelden();
            }

        }


        public Rekening(string nummer, decimal saldo, DateTime creatieDatum, Klant eigenaar)
        {

            this.Nummer = nummer;
            this.Saldo = saldo;
            this.CreatieDatum = creatieDatum;
            Eigenaar = eigenaar;
        }

        public void Lijntrekker(char teken, int lengte)
        {
            for (int teller = 0; teller < lengte; teller++)
            {
                Console.Write(teken);
            }
            Console.WriteLine();
        }

        private Klant eigenaarValue;

        public Klant Eigenaar
        {
            get { return eigenaarValue; }
            set { eigenaarValue = value; }
        }

    }

}


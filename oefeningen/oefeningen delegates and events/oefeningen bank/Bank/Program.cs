﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class Program
    {
        static void Main(string[] args)
        {
            BankBediende bankBediende = new BankBediende("jef", "jeffers");
            Klant klant = new Klant("jan", "jansens");
 
            Zichtrekening MijnZichtrekening = new Zichtrekening("BE74477006250107", 400m, DateTime.Today,-500m, klant);
            Spaarrekening mijnSpaarrekening = new Spaarrekening("BE74477006250107", 1000m, DateTime.Today, klant);
            Kasbon MijnKasbon = new Kasbon(DateTime.Today, 500m, 5, 5m, klant);


            Console.WriteLine("zichtrekening:");
            MijnZichtrekening.Afbeelden();

            Console.WriteLine();

            MijnZichtrekening.RekeningUitreksel += bankBediende.RekeninguitrekselTonen;

            MijnZichtrekening.SaldoInHetRood += bankBediende.SaldoInHetRoodMelden;


            MijnZichtrekening.Storten(300m);
            Console.WriteLine();

            MijnZichtrekening.Afhalen(400m);
            Console.WriteLine();

            MijnZichtrekening.Afhalen(400m);
            Console.WriteLine();


            mijnSpaarrekening.RekeningUitreksel += bankBediende.RekeninguitrekselTonen;
            mijnSpaarrekening.SaldoInHetRood += bankBediende.SaldoInHetRoodMelden;

            Spaarrekening.Intrest = 0.01m;

            Console.WriteLine("spaarrekening");
            mijnSpaarrekening.Afbeelden();
            Console.WriteLine();

            mijnSpaarrekening.Storten(200m);
            Console.WriteLine();

            mijnSpaarrekening.Afhalen(1000m);
            Console.WriteLine();


            mijnSpaarrekening.Afhalen(1500m);
            Console.WriteLine();

           


            


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class Zichtrekening : Rekening
    {
        private decimal maxKredietValue;

        public decimal MaxKrediet
        {
            get { return maxKredietValue; }
            set
            {
                if (value <0m)
                {
                    maxKredietValue = value;
                }
                 }
        }


        public override void Afbeelden()
        {
            base.Afbeelden();
            Console.WriteLine("maxkrediet:{0} euro", MaxKrediet);
        }

        public Zichtrekening(string nummer, decimal saldo, DateTime creatieDatum, decimal maxkrediet, Klant eigenaar) : base(nummer,saldo, creatieDatum,eigenaar)
        {
            MaxKrediet = maxkrediet;
        }



    }
}

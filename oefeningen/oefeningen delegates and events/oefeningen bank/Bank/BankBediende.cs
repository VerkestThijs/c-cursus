﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class BankBediende
    {
        private string voornaamValue;

        public string Voornaam
        {
            get { return voornaamValue; }
            set { voornaamValue = value; }
        }
        private string naamValue;

        public string Naam
        {
            get { return naamValue; }
            set { naamValue = value; }
        }

        public BankBediende(string voornaam, string naam)
        {
            this.Naam = naam;
            this.Voornaam = voornaam;
        }

        public void RekeninguitrekselTonen(Rekening rekening)
        {
            Console.WriteLine("rekening uitteksel van {0}" , rekening.Nummer);
            Console.WriteLine("vorig saldo {0}", rekening.VorigSaldo);
            if (rekening.Saldo > rekening.VorigSaldo)
            {
                Console.WriteLine($"strorting van {rekening.Saldo - rekening.VorigSaldo} euro" );
            }
            else
            {
                Console.WriteLine($"afhaling van {rekening.VorigSaldo -rekening.Saldo}");
            }
            Console.WriteLine($"nieuw saldo is {rekening.Saldo}");
        }

        public override string ToString()
        {
            return $"bankbediende: {Naam} {Voornaam}";
        }




        public void SaldoInHetRoodMelden(Rekening rekening)
        {
            Console.WriteLine("afhaling onmogelijk");
            Console.WriteLine($"het maximum af te halen bedrag bedraagt:{rekening.Saldo} euro");
        }

    }
}

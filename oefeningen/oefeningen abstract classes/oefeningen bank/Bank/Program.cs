﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class Program
    {
        static void Main(string[] args)
        {

            Spaarrekening MijnSpaarrekening = new Spaarrekening("BE74477006250107", 0m, DateTime.Today,4.5m);
            MijnSpaarrekening.Storten(1000m);
            MijnSpaarrekening.Afbeelden();
            MijnSpaarrekening.Lijntrekker('-',25);

            Zichtrekening MijnZichtrekening = new Zichtrekening("BE74477006250107", 0m, DateTime.Today, -1000m);
            MijnZichtrekening.Storten(125m);
            MijnZichtrekening.Afbeelden();
            MijnZichtrekening.Lijntrekker('-',25);

        }
    }
}

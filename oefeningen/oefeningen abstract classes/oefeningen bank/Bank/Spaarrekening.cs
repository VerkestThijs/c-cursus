﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class Spaarrekening :Rekening
    {
        private static decimal intrestValue;

        public static decimal Intrest
        {
            get { return intrestValue; }
            set
            {
                if (value >=0m)
                {
                    intrestValue = value;
                }
                 }
        }

        public override void Afbeelden()
        {
            base.Afbeelden();
            Console.WriteLine("intrest:{0}", Intrest);
        }


        public Spaarrekening(string nummer, decimal saldo, DateTime creatieDatum): base(nummer,saldo,creatieDatum)
        {
            
        }

    }
}

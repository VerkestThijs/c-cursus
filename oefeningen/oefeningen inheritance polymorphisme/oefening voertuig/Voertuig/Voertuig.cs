﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voertuig
{
    public abstract class Voertuig
    {
        private string polisHouderValue;

        public string PolisHouder
        {
            get { return polisHouderValue; }
            set { polisHouderValue = value; }
        }

        private decimal kostprijsValue;

        public decimal Kostprijs
        {
            get { return kostprijsValue; }
            set
            {
                if (value > 0m)
                {
                    kostprijsValue = value;
                }
            }
        }


        private int pkValue;

        public int PK
        {
            get { return pkValue; }
            set
            {
                if (value > 0m)
                {
                    pkValue = value;
                }
            }
        }


        private float gemiddeldVerbruikValue;

        public float GemiddeldeVerbruik
        {
            get { return gemiddeldVerbruikValue; }
            set
            {
                if (value >= 0f)
                {
                    gemiddeldVerbruikValue = value;
                }
            }
        }

        private string nummerplaatValue;

        public string Nummerplaat
        {
            get { return nummerplaatValue; }
            set { nummerplaatValue = value; }
        }

        public virtual void Afbeelden()
        {
            Console.WriteLine("polishouder:{0}", PolisHouder);
            Console.WriteLine("kostprijs: {0}", Kostprijs);
            Console.WriteLine("Pk:{0}", PK);
            Console.WriteLine("gemiddelde verbruik:{0}", GemiddeldeVerbruik);
            Console.WriteLine("nummerplaat:{0}", Nummerplaat);
        }

        public Voertuig() : this("onbepaald", 0m, 0, 0f, "onbepaald")
        {

        }

        public Voertuig(string polishouder, decimal kostprijs, int pk, float gemiddeldverbruik, string nummerplaat)
        {
            this.PolisHouder = polishouder;
            this.Kostprijs = kostprijs;
            this.PK = pk;
            this.GemiddeldeVerbruik = gemiddeldverbruik;
            this.Nummerplaat = nummerplaat;
        }

        public void LijnTekenaar(char teken, int lengte)
        {
            for (int teller = 0; teller < lengte; teller++)
            {
                Console.Write(teken);
            }
            Console.WriteLine();
        }

        public abstract double GetKyototScore();

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class Program
    {
        static void Main(string[] args)
        {
            Klant klant = new Klant("jan", "jansens");
            Spaarrekening MijnSpaarrekening = new Spaarrekening("BE74477006250107", 0m, DateTime.Today, klant);
            Zichtrekening MijnZichtrekening = new Zichtrekening("BE74477006250107", 0m, DateTime.Today, -500m, klant);
            MijnSpaarrekening.Afbeelden();
            MijnZichtrekening.Afbeelden();


        }
    }
}

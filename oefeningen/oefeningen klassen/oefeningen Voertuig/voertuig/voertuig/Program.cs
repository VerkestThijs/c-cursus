﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace voertuig
{
    class Program
    {
        static void Main(string[] args)
        {
            Voertuig v1 = new Voertuig();
            v1.Afbeelden();
            v1.LijnTekenaar('-', 25);
            Voertuig v2 = new Voertuig("jef", 2500m, 50, 9.5f, "000-000");
            v2.Afbeelden();
            v2.LijnTekenaar('-', 25);
            Voertuig v3 = new Voertuig("maarten", -500m, 0, -7f, "001");
            v3.Afbeelden();
            
            
        }
    }
}

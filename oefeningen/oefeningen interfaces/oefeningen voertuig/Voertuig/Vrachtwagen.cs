﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voertuig
{
    public class Vrachtwagen : Voertuig, IVervuiler
    {
        private float maximumLadingValue;

        public float MaximumLading
        {
            get { return maximumLadingValue; }
            set
            {
                if (value > 0f)
                {
                    maximumLadingValue = value;
                }
            }


        }

        public override void Afbeelden()
        {
            base.Afbeelden();
            Console.WriteLine("maxlading: {0}", MaximumLading);
        }

        public Vrachtwagen() : base()
        {
            MaximumLading = 10000;
        }

        public Vrachtwagen(string polishouder, decimal kostprijs, int pk, float gemiddeldverbruik, string nummerplaat, float maximumlading) : base(polishouder, kostprijs, pk, gemiddeldverbruik, nummerplaat)
        {
            this.MaximumLading = maximumlading;
        }

        public override double GetKyototScore()
        {
            double kyotoScore = 0.0;
            if (MaximumLading != 0)
            {
                kyotoScore = (GemiddeldeVerbruik * PK) / (MaximumLading / 1000.0);
            }
            return kyotoScore;
        }

        public double GeefVervuiling()
        {
            return GetKyototScore() * 20;
        }
    }
}

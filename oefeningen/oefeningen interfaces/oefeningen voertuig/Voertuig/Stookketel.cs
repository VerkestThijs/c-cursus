﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voertuig
{
    public class Stookketel : IVervuiler
    {
        private float coNormValue;

        public float CONorm
        {
            get { return coNormValue; }
            set { coNormValue = value; }
        }


        public double GeefVervuiling()
        {
            return CONorm * 100;
        }
        public Stookketel( float conorm)
        {
            this.CONorm = conorm;
        }
    }
}

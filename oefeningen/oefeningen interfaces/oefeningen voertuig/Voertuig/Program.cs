﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voertuig
{
    class Program
    {
        static void Main(string[] args)
        {
            Lijnentrekker v1 = new Lijnentrekker(); // object om lijnlayout te maken

            Console.WriteLine("vervuiling:");
            v1.LijnTekenaar('_', 25);
            IVervuiler[] vervuilers = new IVervuiler[3];
            vervuilers[0] = new Personenwagen("", 15000m, 8, 6.5f, "1-DEF-4156", 5, 5);
            vervuilers[1] = new Vrachtwagen("Jef", 40000m, 500, 30, "1-ABC-123", 10000);
            vervuilers[2] = new Stookketel(7.5f);

            foreach (IVervuiler vervuiler in vervuilers)
            {
                Console.WriteLine("vervuiling: {0}", vervuiler.GeefVervuiling());
                Console.WriteLine();
            }
            


            
            Console.WriteLine("private gegevens :");
            v1.LijnTekenaar('_', 25);
            IPrivaat[] privaats = new IPrivaat[2];
            privaats[0] = (IPrivaat)vervuilers[0];
            privaats[1] = (IPrivaat)vervuilers[1];
            foreach (IPrivaat privaat in privaats)
            {
                Console.WriteLine(privaat.GeefPrivateData());
                Console.WriteLine();
            }
           


            
            Console.WriteLine("MilieuGegeven:");
            v1.LijnTekenaar('_', 25);
            IMilieu[] milieugegevens = new IMilieu[2];
            milieugegevens[0] = (IMilieu)vervuilers[0];
            milieugegevens[1] = (IMilieu)vervuilers[1];
            foreach (IMilieu milieu in milieugegevens)
            {
                Console.WriteLine(milieu.GeefMilieuData());
                Console.WriteLine();
            }
        }
    }
}

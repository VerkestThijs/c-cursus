﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voertuig
{
    public class Personenwagen : Voertuig , IVervuiler
    {
        private int aantalDeurenValue;

        public int AantalDeuren
        {
            get { return aantalDeurenValue; }
            set
            {
                if (value >= 0)
                {
                    aantalDeurenValue = value;
                }
            }
        }

        private int aantalPassagiersValue;

        public int AantalPassagiers
        {
            get { return aantalPassagiersValue; }
            set
            {
                if (value >= 0)
                {
                    aantalPassagiersValue = value;
                }
            }
        }

        public override void Afbeelden()
        {
            base.Afbeelden();
            Console.WriteLine("aantal deuren:{0}", AantalDeuren);
            Console.WriteLine("aantal personen:{0}", AantalPassagiers);
        }

        public Personenwagen() : base()
        {
            AantalDeuren = 4;
            AantalPassagiers = 5;
        }
        public Personenwagen(string polishouder, decimal kostprijs, int pk, float gemiddeldverbruik, string nummerplaat, int aantaldeuren, int aantalpersonen) : base(polishouder, kostprijs, pk, gemiddeldverbruik, nummerplaat)
        {
            AantalDeuren = aantaldeuren;
            AantalPassagiers = aantalpersonen;
        }

        public override double GetKyototScore()
        {
            double kyotoScore = 0.0;
            if (AantalPassagiers!=0)
            {
                kyotoScore = (GemiddeldeVerbruik * PK) / AantalPassagiers;
            }
           
            return kyotoScore;
        }

        public double GeefVervuiling()
        {
           return GetKyototScore()*5;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class Program
    {
        static void Main(string[] args)
        {
            Klant klant = new Klant("jan", "jansens");
            ISpaarmiddel[] spaarmiddel = new ISpaarmiddel[3];
            spaarmiddel[0] = new Zichtrekening("BE74477006250107", 0m, DateTime.Today,-500m, klant);
            spaarmiddel[1] = new Spaarrekening("BE74477006250107", 0m, DateTime.Today, klant);
            spaarmiddel[2] = new Kasbon(DateTime.Today, 500m, 5, 5m, klant);
            foreach (ISpaarmiddel spaarmiddels in spaarmiddel)
            {
                spaarmiddels.Afbeelden();
                Console.WriteLine();
            }


            


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class Kasbon :ISpaarmiddel
    {
        private readonly DateTime eersteAankoop = new DateTime(1900, 1, 1);
        private DateTime aankoopDatumValue;

        public DateTime AankoopDatum
        {
            get { return aankoopDatumValue; }
            set
            {
                if (eersteAankoop < value)
                {
                    aankoopDatumValue = value;
                }
            }
        }

        private decimal bedragValue;

        public decimal Bedrag
        {
            get { return bedragValue; }
            set
            {
                if (value > 0)
                {
                    bedragValue = value;
                }
              }
        }

        private  int looptijdValue;

        public int LoopTijd
        {
            get { return looptijdValue; }
            set
            {
                if (value > 0)
                {
                    looptijdValue = value;
                }
                 }
        }
        private decimal interestVaue;

        public decimal Interest
        {
            get { return interestVaue; }
            set
            {
                if (value > 0)
                {
                    interestVaue = value;
                }
               }
        }
        private Klant eigenaarValue;

        public Klant Eigenaar
        {
            get { return eigenaarValue; }
            set { eigenaarValue = value; }
        }

        public void Afbeelden()
        {
            if (Eigenaar!= null)
            {
                Console.WriteLine($"eigenaar : {Eigenaar.ToString()}");
                Eigenaar.Afbeelden();
            }
            Console.WriteLine("aankoopdatum:{0}", AankoopDatum);
            Console.WriteLine("bedrag: {0} euro",Bedrag);
            Console.WriteLine("looptijd: {0} jaar",LoopTijd);
            Console.WriteLine($"interest:{Interest}%");
            
        }

        public Kasbon(DateTime aankoopdatum, decimal bedrag, int looptijd, decimal interest, Klant eigenaar)
        {
            this.AankoopDatum = aankoopdatum;
            this.Bedrag = bedrag;
            this.LoopTijd = looptijd;
            this.Interest = interest;
            this.Eigenaar = eigenaar;
        }
    }
}

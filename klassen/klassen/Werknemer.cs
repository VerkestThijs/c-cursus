﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klassen
{
    public class Werknemer
    {
        private string naamValue;

        public string Naam
        {
            get { return naamValue; }
            set
            {
                if (value != string.Empty)
                {
                    naamValue = value;
                }

            }
        }

        private DateTime inDienstValue;

        public DateTime InDienst
        {
            get { return inDienstValue; }
            set { inDienstValue = value; }
        }

        private Geslacht geslachtValue;

        public Geslacht Geslacht
        {
            get { return geslachtValue }
            set
            {
                geslachtValue = value;
            }
        }

        private decimal salarisValue;
        public decimal Salaris
        {
            get
            {
                return salarisValue;
            }
            private set
            {
                if (value >= 0m)
                {
                    salarisValue = value;
                }
            }
        }
    }
}

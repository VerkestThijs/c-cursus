﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klassen
{
    class Program
    {
        static void Main(string[] args)
        {
            Werknemer ik;
            ik = new Werknemer();
            ik.Naam = "asterix";
            ik.Geslacht = Geslacht.Man;
            ik.InDienst = new DateTime(2018, 1, 1);
            Console.WriteLine(ik.Naam);
            Console.WriteLine(ik.Geslacht);
            Console.WriteLine(ik.InDienst);
        }
    }
}

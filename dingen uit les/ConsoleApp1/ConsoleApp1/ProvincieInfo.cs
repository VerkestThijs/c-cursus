﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp1
{
    public class ProvincieInfo
    {
        public int ProvincieGrootte(string provincieNaam)
        {

                StreamReader lezer = new StreamReader(@"D:\bitbucketcursus\c-cursus\dingen uit les\ConsoleApp1\ConsoleApp1\provincies.txt");
            try
            {
                
                string regel;
                while ((regel = lezer.ReadLine()) != null)
                {
                    int dubbelPuntPos = regel.IndexOf(':');
                    string provincie = regel.Substring(0, dubbelPuntPos);
                    if (provincie == provincieNaam)
                    {
                        return int.Parse(regel.Substring(dubbelPuntPos + 1));

                    }
                }
            }
            finally
            {
                lezer.Close();
                throw new Exception("onbestaande provincie:" + provincieNaam);
            }
            
                
        }
    }
}

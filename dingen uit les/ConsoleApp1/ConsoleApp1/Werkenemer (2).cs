﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   public class Werkenemer
    {
        private static DateTime personeelsFeestValue;

        public static DateTime PersoneelsFeest
        {
            get { return personeelsFeestValue; }
            set { personeelsFeestValue = value; }
        }


        private DateTime inDienstValue;

        public DateTime inDienst
        {
            get { return inDienstValue; }
            set { inDienstValue = value; }
        }

        private string naamValue;

        public string Naam
        {
            get { return naamValue; }
            set {if(value!=string.Empty)
                naamValue = value; }
        }

        private Geslacht geslachtValue;

        public Geslacht Geslacht
        {
            get { return geslachtValue; }
            set
            {
            }
        }

        private decimal salarisValue;

        public decimal Salaris
        {
            get { return salarisValue; }
            set { if(value >=0m)
                salarisValue = value; }
        }

        public bool VerjaarAncien
        {
            get
            {
                return inDienstValue.Month == DateTime.Today.Month && inDienstValue.Day == DateTime.Today.Day;
            }
        }



        public virtual void Afbeelden()
        {
            Console.WriteLine("Naam: {0}", Naam);
            Console.WriteLine("Geslacht : {0}",Geslacht);
            Console.WriteLine("In Dienst:{0}",inDienst);
            Console.WriteLine("personeelsfeest :{0}", PersoneelsFeest);
        }

        public Werkenemer()
        {
          this.Naam = "onbekend";
            this.inDienst = DateTime.Today;
        }
        public Werkenemer(string naam, DateTime inDienst,Geslacht geslacht)
        {
            this.Naam = naam;
            this.inDienst = inDienst;
            Geslacht = geslacht;
        }


        static Werkenemer()
        {
            PersoneelsFeest = new DateTime(DateTime.Today.Year, 2, 1);
            while (PersoneelsFeest.DayOfWeek != DayOfWeek.Friday)
            {
                PersoneelsFeest = PersoneelsFeest.AddDays(1);
            }
        }
    }
}

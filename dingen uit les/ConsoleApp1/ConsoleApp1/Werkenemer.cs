﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firma.Personeel
{
    public abstract class Werkenemer :IKost
    {
        public static void UitgebreideWerknemersLijst(Werkenemer[] werkenemers)
        {
            Console.WriteLine("uitgebreide werknemerslijst:");
            foreach (Werkenemer werkenemer in werkenemers)
            {
                werkenemer.Afbeelden();
            }
        }

        public static void KorteWerknemersLijst(Werkenemer[] werkenemers)
        {
            Console.WriteLine("Korte werknemerslijst:");
            foreach (Werkenemer werkenemer in werkenemers)
            {
                Console.WriteLine(werkenemer.ToString());
            }
        }


        private DateTime[] verlofdagenValue;    

        public DateTime[] VerlofDagen
        {
            get { return verlofdagenValue; }
            set { verlofdagenValue = value; }
        }

        private DateTime[] ziektedagenValue;

        public DateTime[] Ziektedagen
        {
            get { return ziektedagenValue; }
            set { ziektedagenValue = value; }
        }





        private Afdeling afdelingValue;

        public Afdeling Afdeling
        {
            get { return afdelingValue; }
            set { afdelingValue = value; }
        }


        public abstract decimal Premie { get; }
        private DateTime inDienstValue;

        public DateTime inDienst
        {
            get { return inDienstValue; }
            set { inDienstValue = value; }
        }

        private string naamValue;

        public string Naam
        {
            get { return naamValue; }
            set
            {
                if (value != string.Empty)
                    naamValue = value;
            }
        }

        private Geslacht geslachtValue;

        public Geslacht Geslacht
        {
            get { return geslachtValue; }
            set
            {
            }
        }

        private decimal salarisValue;

        public decimal Salaris
        {
            get { return salarisValue; }
            set
            {
                if (value >= 0m)
                    salarisValue = value;
            }
        }

        public bool VerjaarAncien
        {
            get
            {
                return inDienstValue.Month == DateTime.Today.Month && inDienstValue.Day == DateTime.Today.Day;
            }
        }

        private static DateTime personeelsfeestValue;

        public static DateTime personeelsfeest
        {
            get { return personeelsfeestValue; }
            set { personeelsfeestValue = value; }
        }

        public abstract decimal Bedrag { get;}

        public bool Menselijk { get { return true; } }

        public Werkenemer(string naam, DateTime inDienst, Geslacht geslacht)
        {
            this.Naam = naam;
            this.inDienst = inDienst;
            this.Geslacht = geslacht;
        }



        public virtual void Afbeelden()
        {
            Console.WriteLine("Naam: {0}", Naam);
            Console.WriteLine("Geslacht : {0}", Geslacht);
            Console.WriteLine("In Dienst:{0}", inDienst);
            Console.WriteLine("Personeelsfeest : {0}", personeelsfeest);
            if (Afdeling != null)
            {
                Console.WriteLine(Afdeling);
            }
        }

        static Werkenemer()
        {
            personeelsfeest = new DateTime(DateTime.Today.Year, 2, 1);
            while (personeelsfeest.DayOfWeek != DayOfWeek.Friday)
                personeelsfeest = personeelsfeest.AddDays(1);
        }

        public override string ToString()
        {
            return Naam + ' ' + Geslacht;
        }

        public override bool Equals(object obj)
        {
            if (obj is Werkenemer)
            {
                Werkenemer deAndere = (Werkenemer)obj;
                return this.Naam == deAndere.Naam;
            }

            else
                return false;


        }

        public override int GetHashCode()
        {
            return Naam.GetHashCode();
        }


        public class WerkRegime
        {
            public enum RegimeType
            {
                Voltijds,
                Viertijds,
                Halftijds
            }

            private RegimeType typeValue;

            public RegimeType Type
            {
                get { return typeValue; }
                set { typeValue = value; }
            }

            public int AantalVakantiedagem
            {
                get
                {
                    switch (Type)
                    {
                        case RegimeType.Voltijds:
                            return 25;
                        case RegimeType.Viertijds:
                            return 20;
                        case RegimeType.Halftijds:
                            return 12;
                        default:
                            return 0;
                    }

                }

            }

            public WerkRegime( RegimeType type)
            {
                Type = type;
            }

            public override string ToString()
            {
                return Type.ToString();
            }


        }
    }

}

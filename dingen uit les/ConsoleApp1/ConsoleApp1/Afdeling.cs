﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firma.Personeel
{
    public class Afdeling
    {
        private string naamValue;

        public string Naam
        {
            get { return naamValue; }
            set { naamValue = value; }
        }

        private int verdiepingValue;

        public int Verdieping
        {
            get { return verdiepingValue; }
            set
            {
                if (value >=0  && value <= verdiepingen)
                {
                    verdiepingValue = value;
                }
            }
        }

        public const int verdiepingen = 10;

        public override string ToString()
        {
            return string.Format($"afdeling:{Naam} op verdieping {Verdieping}");
           //return string.Format("afdeling:{0} op verdieping{1}", Naam, verdiepingen);
        }

        public Afdeling(string naam, int verdieping)
        {
            this.Naam = naam;
            Verdieping = verdieping;
        }


    }
}

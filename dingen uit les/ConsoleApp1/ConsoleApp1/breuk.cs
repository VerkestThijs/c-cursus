﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   public class Breuk
    {
        private int tellerValue;

        public int Teller
        {
            get { return tellerValue; }
            set { tellerValue = value; }
        }

        private int noemerValue;

        public int Noemer
        {
            get { return noemerValue; }
            set
            {
                if (value == 0)
                {
                    throw new Exception("noemer mag niet nul zijn");
                }
                noemerValue = value; }
        }

        public Breuk( int teller,int noemer)
        {
            Teller = teller;
            Noemer = noemer;
                     
        }
        public override string ToString()
        {
            return $"{Teller}/{Noemer}";
        }

        public override bool Equals(object obj)
        {
            if (obj is Breuk)
            {
                Breuk andereBreuk = (Breuk)obj;
                return (decimal)Teller / Noemer == (decimal)andereBreuk.Teller / andereBreuk.Noemer;
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return Teller + Noemer;
        }
        public static bool operator == (Breuk eerste, Breuk tweede)
        {
            return eerste.Equals(tweede);
        }
        public static bool operator !=(Breuk eerste, Breuk tweede)
        {
            return !eerste.Equals(tweede);
        }

        public static Breuk operator *(Breuk eerste, Breuk tweede)
        {
            return new Breuk(
                eerste.Teller * tweede.Teller, eerste.Noemer * tweede.Noemer);

        }
        public static Breuk operator ++(Breuk breuk)
        {
            return new Breuk(breuk.Teller + breuk.Noemer, breuk.Noemer);
        }


    }
}

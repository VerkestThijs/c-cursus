﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firma.Materiaal

{
    public delegate void Onderhoudsbeurt(Fotokopiemachine machine);

    public class Fotokopiemachine : IKost
    {
        public event Onderhoudsbeurt OnderhoudNodig;

        private const int AantalBlzTussen20OnderhoudsBeurten = 10;

        public void Fotokopieer(int aantalBlz)
        {
            for (int blz = 1; blz <= aantalBlz; blz++)
            {
                Console.WriteLine($"fotokopieerMachine {SerieNr} kopieert " +
                    $" blz. {blz} van {aantalBlz}");
                if (++AantalGekopieerdeBlz % AantalBlzTussen20OnderhoudsBeurten == 0)
                {
                    if (OnderhoudNodig != null)
                    {
                        OnderhoudNodig(this);
                    }
                }
            }
        }

        private string serieNrValue;

        public string SerieNr
        {
            get { return serieNrValue; }
            set { serieNrValue = value; }
        }
        private int aantalGekopieerdeBlzValue;

        public int AantalGekopieerdeBlz
        {
            get { return aantalGekopieerdeBlzValue; }
            set
            {
                {
                    if (value < 0)
                    {
                        throw new AantalGekopieerdeBlzException ("aantal gekopieerde blz <0",value);
                        
                    }
                    aantalGekopieerdeBlzValue = value;
                }
            }
        }

        private decimal kostPerBlzValue;

        public decimal KostPerBlz
        {
            get { return kostPerBlzValue; }
            set
            {
                if (value <= 0)
                {
                    throw new KostPerBlzException ("kost per blz is <= 0",value);
                }
                kostPerBlzValue = value;
            }
        }

        public Fotokopiemachine(string serieNr, int aantalgekopierdeBlz, decimal kostPerBlz)
        {
            this.SerieNr = serieNr;
            AantalGekopieerdeBlz = aantalgekopierdeBlz;
            KostPerBlz = kostPerBlz;
        }


        public decimal Bedrag
        {
            get
            {
                return AantalGekopieerdeBlz * KostPerBlz;
            }
        }

        public bool Menselijk
        {
            get
            {
                return false;
            }
        }


        public class KostPerBlzException: Exception
        {
            private decimal verkeerdeKostValue;

            public decimal VerkeerdeKost
            {
                get { return verkeerdeKostValue; }
                set { verkeerdeKostValue = value; }
            }

            public KostPerBlzException(string message, decimal verkeerdekost) :base(message)
            {
                VerkeerdeKost = verkeerdekost;
            }



        }

        public class AantalGekopieerdeBlzException : Exception
        {
            private int verkeerdAantalBlz;      

            public int VerkeerdAantalBlz
            {
                get { return verkeerdAantalBlz; }
                set { verkeerdAantalBlz = value; }
            }

            public AantalGekopieerdeBlzException(string message, int verkeerdAantalBlz):base (message)
            {
                VerkeerdAantalBlz = verkeerdAantalBlz;
            }

        }
    }
}

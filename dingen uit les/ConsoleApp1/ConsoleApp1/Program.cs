﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Firma;
using Firma.Materiaal;
using Firma.Personeel;
using MateriaalStatus = Firma.Materiaal.Status;
using PersoneelStatus = Firma.Personeel.Status;


namespace ConsoleApp1
{
    class Program
    {
        delegate int FunctieMetTweeParameters(int getal1, int getal2);
        delegate int functieMetEenParameter(int getal);
        delegate int functieZonderParameter();

        delegate bool Filter(int getal);

        private static bool IsEvenGetal(int getal)
        {
            return (getal % 2 == 0);
        }
        private static bool IsOnevenGetal(int getal)
        {
            return getal % 2 == 1;
        }

        delegate void Werknemerslijst(Werkenemer[] werkenemers);

        static void Main(string[] args)
        {
            /*
            Werkenemer ik = new Werkenemer();
            ik.Naam = "Asterix";
            ik.Geslacht = Geslacht.Man;
            ik.inDienst = new DateTime(2018, 1, 1);
            Console.WriteLine(ik.Naam);
            Console.WriteLine(ik.Geslacht);
            Console.WriteLine(ik.inDienst);

    

            Werkenemer ik = new Werkenemer();
            if (ik == null)
            {
                Console.WriteLine("Niet Verbonden");
            }
            else
            {
                Console.WriteLine("verbonden");
            }
            ik = null;
            if (ik == null)
            {
                Console.WriteLine("Niet Verbonden");
            }
            else
            {
                Console.WriteLine("verbonden");
            }

    

            Werkenemer ik = new Werkenemer();
            Console.WriteLine( ik == null ? "niet verbinden" : "verbinden");
            ik = null;
            Console.WriteLine(ik == null ? "niet verbinden"  : "verbinden");

    

            Werkenemer ik = new Werkenemer();
            ik.Naam = "Asterix";
            ik.Geslacht = Geslacht.Man;
            ik.inDienst = new DateTime(2018, 1, 1);
            Console.WriteLine(ik.Naam);
            Werkenemer mezelf;
            mezelf = ik;
            Console.WriteLine(mezelf.Naam);
            Console.WriteLine(ik == mezelf);
            ik = null;
            Console.WriteLine(ik == mezelf);
            Console.WriteLine(mezelf.Naam);
            

            Werkenemer[] OnzeWerknemers = new Werkenemer[20];
            for (int teller = 0; teller < OnzeWerknemers.Length; teller++)
            {
                OnzeWerknemers[teller] = new Werkenemer();
            }

    

            Werkenemer ik = new Werkenemer();
            ik.Naam = "Asterix";
            ik.Geslacht = Geslacht.Man;
            ik.inDienst = new DateTime(2018, 12, 19); 
            Console.WriteLine(ik.VerjaarAncien);
            ik.Afbeelden();


    
            Werkenemer ik = new Werkenemer();
            ik.Naam = "Asterix";
            ik.Geslacht = Geslacht.Man;
            ik.inDienst = new DateTime(2018, 12, 19);
            Werkenemer jij = new Werkenemer();
            jij.Naam = "Obelix";
            jij.Geslacht = Geslacht.Man;
            jij.inDienst = new DateTime(2018, 1, 2);
            LijnenTrekker lijnenTrekker = new LijnenTrekker();
            ik.Afbeelden();
            lijnenTrekker.TrekLijn(30,'+');
            jij.Afbeelden();
            lijnenTrekker.TrekLijn(79,'*');
            lijnenTrekker.TrekLijn();
            lijnenTrekker.TrekLijn(10);
            lijnenTrekker.TrekLijn();
            Console.ReadLine();

            
            Arbeider ik = new Arbeider("asterix", new DateTime(2018,1,1),Geslacht.Man,24.79m,3);
            ik.Afbeelden();
            

            Bediende ik = new Bediende("asterix", new DateTime(2018, 1, 1), Geslacht.Man, 2400.79m);
            ik.Afbeelden();


            Manager jij = new Manager("Obelix", new DateTime(2018, 1, 1), Geslacht.Man, 2400.79m, 7000m);
            jij.Afbeelden();
            Console.WriteLine(jij.ToString()); 
            

            Manager jij = new Manager("asterix", new DateTime(2018, 1, 1), Geslacht.Man, 2400.79m, 7000m);
            {
                Console.WriteLine(jij.ToString());
            }


            Manager ik = new Manager("asterix", new DateTime(2018, 1, 1), Geslacht.Man, 2400.79m, 7000m);
            {
                Console.WriteLine(ik);
            }

            // bovengestelde wat meer vragen over stellen. waarom heeft writeline dezelfde output als to string.
            
            Manager ik = new Manager("asterix", new DateTime(2018, 1, 1), Geslacht.Man, 2400.79m, 7000m);
            Manager mezelf = ik;
            Manager dezelfde = new Manager("asterix", new DateTime(2018, 1, 1), Geslacht.Man, 2400.79m, 7000m);
            Console.WriteLine(ik.Equals(mezelf));
            Console.WriteLine(ik.Equals(dezelfde));
            

            object ik = new Manager("Asterix", new DateTime(2018, 1, 1), Geslacht.Man, 2400.79m, 7000m);
            Console.WriteLine(ik is Manager);
            Console.WriteLine(ik is Bediende);
            Console.WriteLine(ik is Werkenemer);
            Console.WriteLine(ik is Arbeider);
            Console.WriteLine(ik is string);
            
            Werkenemer ik = new Bediende("Asterix", DateTime.Today, Geslacht.Man, 1500m);
            Werkenemer jij = new Arbeider("obelix", DateTime.Today, Geslacht.Man, 10m, 1);

            Bediende hij;
            hij = (Bediende)ik;
            hij.Afbeelden();

            hij = ik as Bediende;
            if (hij != null)
            {
                hij.Afbeelden();
            }

            hij = jij as Bediende;
            if (hij != null)
            {
                hij.Afbeelden();
            }

            

            Werkenemer ik = new Manager("asterix",new DateTime(2018,1,1),Geslacht.Man,2400.97m,7000m);
            

            Arbeider asterix = new Arbeider("asterix", new DateTime(2018, 1, 1), Geslacht.Man, 24.79m, 3);
            Bediende obelix = new Bediende("obelix", new DateTime(1995, 1, 1), Geslacht.Man, 2400.79m);
            Manager idefix = new Manager("idefix", new DateTime(1996, 1, 1), Geslacht.Man, 2400.79m, 7000m);
            Console.WriteLine(asterix.Premie);
            Console.WriteLine(obelix.Premie);
            Console.WriteLine(idefix.Premie);
            


            Werkenemer[] wij = new Werkenemer[3];
            wij[0] = new Arbeider("asterix", new DateTime(1990, 1, 1), Geslacht.Man, 24.79m, 3);
            wij[1] = new Bediende("obelix", new DateTime(1990, 1, 1), Geslacht.Man, 2400m);
            wij[2] = new Manager("idefix", new DateTime(1455, 1, 1), Geslacht.Man, 2400m, 9000m);
            
            foreach (Werkenemer werkenemer in wij)
            {
                werkenemer.Afbeelden();
                
            }
            

            for (int i = 0; i < wij.Length; i++)
            {
                wij[i].Afbeelden();
            }

            


            Afdeling afdeling1 = new Afdeling("strijd", 0);
            Afdeling afdeling2 = new Afdeling("feest", 1);

            Werkenemer[] wij = new Werkenemer[3];
            wij[0] = new Arbeider("asterix", new DateTime(1990, 1, 1), Geslacht.Man, 24.79m, 3);
            wij[0].Afdeling = afdeling1;
            wij[1] = new Bediende("obelix", new DateTime(1990, 1, 1), Geslacht.Man, 2400m);
            wij[1].Afdeling = afdeling1;
            wij[2] = new Manager("idefix", new DateTime(1455, 1, 1), Geslacht.Man, 2400m, 9000m);
            wij[2].Afdeling = afdeling2;

            foreach (Werkenemer werkenemer in wij)
            {
                werkenemer.Afbeelden();

            }


           

            IKost[] kosten = new IKost[4];
            kosten[0] = new Arbeider("asterix", new DateTime(1990, 1, 1), Geslacht.Man, 24.79m, 3);
            kosten[1] = new Bediende("obelix", new DateTime(1990, 1, 1), Geslacht.Man, 2400m);
            kosten[2] = new Manager("idefix", new DateTime(1455, 1, 1), Geslacht.Man, 2400m, 9000m);
            kosten[3] = new Fotokopiemachine("123", 500, 0.025m);

            decimal totaleKost = 0m;

            foreach (IKost kost in kosten)
            {
                Console.WriteLine(kost.Menselijk);
                Console.WriteLine(kost.Bedrag);
                totaleKost += kost.Bedrag;
            }
            Console.WriteLine(totaleKost);
             


        

            Werkenemer asterix = new Arbeider("Asterix", new DateTime(2018, 1, 1), Geslacht.Man, 24.79m, 3);
            asterix.VerlofDagen = new DateTime[] { new DateTime(2018, 2, 1), new DateTime(2018, 2, 2), new DateTime(2018, 5, 11) };
            asterix.Ziektedagen = new DateTime[] { new DateTime(2018, 6, 4), new DateTime(2018, 6, 5), new DateTime(2018, 6, 6), new DateTime(2018, 6, 7), new DateTime(2018, 6, 8) };

            Werkenemer obelix = new Bediende("Obelix", new DateTime(1995, 2, 1), Geslacht.Man, 2400.79m);
            obelix.VerlofDagen = new DateTime[] { new DateTime(2018, 7, 9), new DateTime(2018, 7, 10), new DateTime(2018, 7, 11), new DateTime(2018, 7, 12), new DateTime(2018, 7, 13) };
            obelix.Ziektedagen = new DateTime[] { new DateTime(2018, 8, 1), new DateTime(2018, 8, 2), new DateTime(2018, 8, 3) };

            (int aantalVerlofdagen, int aantalZiektedagen) afwezighedenAsterix = Afwezigheden(asterix);

            Console.WriteLine($"Asterix: " + 
                $"{afwezighedenAsterix.aantalVerlofdagen} verlofdagen + " +
                $"{afwezighedenAsterix.aantalZiektedagen} ziektedagen");

            var afwezighedenObelix = Afwezigheden(obelix);
            Console.WriteLine($"Obelix: " + 
                $"{afwezighedenObelix.aantalVerlofdagen} verlofdagen +" +
                $" {afwezighedenObelix.aantalZiektedagen} ziektedagen");

            (int aantalVerlofdagen, int aantalZiektedagen) = Afwezigheden(obelix);
            Console.WriteLine($"Obelix: " +
                $"{aantalVerlofdagen} verlofdagen + " +
                $"{aantalZiektedagen} ziektedagen");
            


            MateriaalStatus statusBoorMachine = MateriaalStatus.Werkend;
            PersoneelStatus statusChef = PersoneelStatus.HogerKader;
            Console.WriteLine(statusBoorMachine);
            Console.WriteLine(statusChef); 

            

            Werkenemer[] wij = new Werkenemer[3];
            wij[0] = new Arbeider("asterix", new DateTime(1990, 1, 1), Geslacht.Man, 24.79m, 3);
            wij[1] = new Bediende("obelix", new DateTime(1990, 1, 1), Geslacht.Man, 2400m);
            wij[2] = new Manager("idefix", new DateTime(1455, 1, 1), Geslacht.Man, 2400m, 9000m);

            Werknemerslijst lijst;

            lijst = Werkenemer.UitgebreideWerknemersLijst;
            lijst(wij);
            Console.WriteLine();

            lijst = Werkenemer.KorteWerknemersLijst;
            lijst(wij);


            

            Fotokopiemachine machine = new Fotokopiemachine("123", 0, 2.0m);
            Fotokopiemachine machine1 = new Fotokopiemachine("456", 0, 2.5m);
            Bediende eenBediende = new Bediende("asterix", new DateTime(1988, 1, 1), Geslacht.Man, 2400.79m);
            Manager eenManager = new Manager("idefix", DateTime.Today, Geslacht.Man, 4800m, 2000m);
            machine.OnderhoudNodig += eenBediende.DoeOnderhoud;
            machine.OnderhoudNodig += eenManager.OnderhoudNoteren;
            machine1.OnderhoudNodig += eenBediende.DoeOnderhoud;
            machine1.OnderhoudNodig += eenManager.OnderhoudNoteren;
            machine.Fotokopieer(45);
            machine1.Fotokopieer(30);

            




            Werkenemer[] wij = new Werkenemer[3];
            wij[0] = new Arbeider("asterix", new DateTime(1990, 1, 1), Geslacht.Man, 24.79m, 3);
            wij[1] = new Bediende("obelix", new DateTime(1990, 1, 1), Geslacht.Man, 2400m);
            wij[2] = new Manager("idefix", new DateTime(1455, 1, 1), Geslacht.Man, 2400m, 9000m);

            Werknemerslijst rapport;

            rapport = delegate (Werkenemer[] werkenemers)
            {
                decimal totaal = 0m;
                foreach (Werkenemer werkenemer in werkenemers)
                {
                    totaal += werkenemer.Bedrag;
                }
                Console.WriteLine($"totale kost is {totaal}");
            };
            rapport(wij);

            

            Fotokopiemachine machine = new Fotokopiemachine("123", 0, 2.0m);
            Bediende eenBediende = new Bediende("asterix", new DateTime(1988, 1, 1), Geslacht.Man, 2400.79m);

            machine.OnderhoudNodig += delegate (Fotokopiemachine apparaat)
            {
                Console.WriteLine($" onderhoud is aangevraagd voor machine {apparaat.SerieNr}");
            };

            machine.OnderhoudNodig += eenBediende.DoeOnderhoud;
            machine.Fotokopieer(45);

            


            try
            {
                Fotokopiemachine machine = new Fotokopiemachine("123", 100, -5.4m);
                Console.WriteLine("machine goed ingevuld");
            }
            catch(Fotokopiemachine.AantalGekopieerdeBlzException ex)
            {
                Console.WriteLine("fout:"+ ex.Message + ':' + ex.VerkeerdAantalBlz);
            }
            catch(Fotokopiemachine.KostPerBlzException ex)
            {
                Console.WriteLine("fout:" +ex.Message + ':' + ex.VerkeerdeKost);
            }
            Console.WriteLine("einde programma");

            




            Console.Write("Provincie: ");
            string provincie = Console.ReadLine();
            try
            {
                ProvincieInfo info = new ProvincieInfo();
                Console.WriteLine(info.ProvincieGrootte(provincie));
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }



            

            Breuk breuk1 = new Breuk(1, 2);
            Breuk breuk2 = new Breuk(1, 3);
            Breuk breuk3 = new Breuk(2, 6);
            // Breuk breuk4 = breuk1++;
            //Console.WriteLine(breuk1.ToString());
            //Console.WriteLine(breuk2.Equals(breuk3));
            //Console.WriteLine(breuk1 == breuk2);
            //Console.WriteLine(breuk2== breuk3);
            //Console.WriteLine(breuk1*breuk2);
            //Console.WriteLine(breuk1);
            //Console.WriteLine(breuk4); 

            breuk1 *= breuk2;
            Console.WriteLine(breuk1);

            


            Overuren mijnOveruren = new Overuren();
            mijnOveruren[0] = 4;
            mijnOveruren["apr"] = 2;
            Console.WriteLine(mijnOveruren["jan"]);
            Console.WriteLine(mijnOveruren[3]);
            Console.WriteLine(mijnOveruren.Totaal);


            string module = "C# PF";
            Console.WriteLine(module[0]);
            



            Arbeider asterix = new Arbeider("Asterix", new DateTime(2018, 1, 1), Geslacht.Man, 24.79m, 3);
            Bediende obelix = new Bediende("Obelix", new DateTime(2018, 1, 1), Geslacht.Man, 2400.79m);
            Manager idefix = new Manager("Idefix", new DateTime(2018, 1, 1), Geslacht.Man, 2400.79m, 7000m);

            List<Werkenemer> personeel = new List<Werkenemer>();
            personeel.Add(asterix);
            personeel.Add(obelix);
            personeel.Insert(1, idefix);

            Console.WriteLine($"{personeel[0].Naam}"+
                $" is de 1ste van {personeel.Count} personeelsleden.");

            foreach (Werkenemer personeelslid in personeel)
            {
                Console.WriteLine(personeelslid.Naam);
            }

            


            List<Werkenemer> werkenemers = new List<Werkenemer>
            {
                new Arbeider("asterix",DateTime.Today, Geslacht.Man,25m,1),
                new Bediende("obelix",DateTime.Today,Geslacht.Man,1200m)

            };

            Console.WriteLine($"aantal werknemers: {werkenemers.Count}");

            werkenemers = null;
            //Console.WriteLine($"aantal werknemers: {werkenemers.Count}");
            Console.WriteLine($"aantal werknemers: {werkenemers?.Count ?? 0}");

            



            byte? aantalkinderen =null;
            byte aantalKamers;
            aantalKamers = aantalkinderen ?? 0;
            Console.WriteLine($"er zijn {aantalKamers} kinderkamers nodig");
            


            var aantalkinderen = 3;
            var wedde = 1500m;
            var namen = new List<string>();

            Console.WriteLine(aantalkinderen.GetType());
            Console.WriteLine(wedde.GetType());
            Console.WriteLine(namen.GetType());
            

            List<Object> lijst = new List<Object>()
            {
                new Arbeider("Asterix", new DateTime(2018, 1, 1), Geslacht.Man, 24.79m, 3),
                new Bediende("Obelix", new DateTime(2018, 2, 1), Geslacht.Man, 2400.79m),
                new Fotokopiemachine("123", 500, 0.025m),
                null,
                "C# 7.0"
            };
            foreach (var item in lijst)
            {
                ToonGegevens(item);
            }
            

            // switch patern matching nog doen       


            Filter evenGetal = IsEvenGetal;
            Console.WriteLine("even getallen:");
            ToonGetallen(evenGetal);


            Filter onevenGetal = IsOnevenGetal;
            Console.WriteLine("oneven getallen");
            ToonGetallen(onevenGetal);

            Filter getalDeelbaarVijf = delegate (int getal)
            {
                return (getal % 5 == 0);
                //||getal%2==0);
            };
            Console.WriteLine("getallen deelbaar door 5:");
            ToonGetallen(getalDeelbaarVijf);


            //DelegateType kwadraat = getal => getal * getal;
            //Console.WriteLine(kwadraat(10));

            FunctieMetTweeParameters som = (getal1, getal2) => getal1 + getal2;
            Console.WriteLine(som(3,7));
            Console.WriteLine(som(10,6));

            functieMetEenParameter kwadraat = (getal) => getal * getal;
            Console.WriteLine(kwadraat(5));

            functieZonderParameter willekeurigGetal = () => new Random().Next(10);
            Console.WriteLine(willekeurigGetal());

            */

            Filter filterEvenGetalen = getal => getal % 2 == 0;
            Console.WriteLine("even getallen:");
            ToonGetallen(filterEvenGetalen);

            Console.WriteLine("Oneven getallen :");
            ToonGetallen(getal => getal % 2 == 1);

            Console.WriteLine("getallen deelbaar door 5");
            ToonGetallen(getal => getal % 5 == 0);

            Console.WriteLine("------------");

            ToonGetallen(MaakLambda());

            Action<int> kwdraat = getal => Console.WriteLine(getal * getal); ;
            kwdraat(10);

            Func<int, int, int> som = (getal, getal2) => getal + getal2;
            Console.WriteLine(som(10, 5));

            Func<string, int, string> tekstdeel = (tekst, vanaf) => tekst.Substring(vanaf);

            Console.WriteLine(tekstdeel("vdab", 2));

            Action<string, int> tekstdeel2 = (tekst, vanaf) => Console.WriteLine(tekst.Substring(vanaf));
            tekstdeel("vdab", 2);

            int resultaat = som(4, 5);
            Console.WriteLine(resultaat);












        }
        private static int som(int getal1, int getal2) => getal1 + getal2;

        static Filter MaakLambda()
        {
            Console.WriteLine("geef een getal in");
            var deelbaarDoor = int.Parse(Console.ReadLine());
            return getal => getal % deelbaarDoor == 0;
        }

        private static void ToonGetallen(Filter filter)
        {
            var getallen = new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            foreach (var getal in getallen)
            {
                if (filter(getal))
                    Console.WriteLine(getal);
            }
        }


        private static void ToonGegevens(object obj)
        {
            if (obj is Werkenemer w)
            {
                Console.WriteLine($"werknemer {w.Naam} kost {w.Bedrag} euro");
            }
            else if (obj is Fotokopiemachine f)
            {
                Console.WriteLine($"fotokopiemachine {f.SerieNr} kopieerde  {f.AantalGekopieerdeBlz} en kost{f.Bedrag}");
            }
            else
            {
                Console.WriteLine("onbekend type");
            }
        }


       /* private static void ToonGegevens(object obj)
        {
            if (obj is Werkenemer)
            {
                Werkenemer w = obj as Werkenemer;
                Console.WriteLine($"werknemer {w.Naam} kost {w.Bedrag} euro");
            }
            else if (obj is Fotokopiemachine)
            {
                Fotokopiemachine f = (Fotokopiemachine)obj;
                Console.WriteLine($"fotokopiemachine {f.SerieNr} kopieerde  {f.AantalGekopieerdeBlz} en kost{f.Bedrag}");
            }
            else
            {
                Console.WriteLine("onbekend type");
            }
        }

        
        
        public static (int aantalVerlofdagen, int aantalZiektedagen) Afwezigheden(Werkenemer werkenemer)
        {
            (int, int) aantalAfwezigeheden = (werkenemer.VerlofDagen.Length, werkenemer.Ziektedagen.Length);
            return aantalAfwezigeheden;
        }

        */

    }
}

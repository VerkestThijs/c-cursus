﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht
{
    interface IVoorwerpen
    {
        void GegevensTonen();

        decimal Winst { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht
{
    public class Leesboek : Boek
    {
        public override decimal Winst
        {
            get
            {
                return Aankoopprijs * 1.5m;
            }
        }

        private string onderwerpValue;

        public string Onderwerp
        {
            get { return onderwerpValue; }
            set { onderwerpValue = value; }
        }

        public override void GegevensTonen()
        {
            base.GegevensTonen();
            Console.WriteLine($"winst: {Winst}");
            Console.WriteLine($"onderwerp: {Onderwerp}");
        }


        public Leesboek(string titel,string auteur, decimal aankoopprijs, Genre genre, string onderwerp):base(titel,auteur,aankoopprijs,genre)
        {
            Onderwerp = onderwerp;
        }
    }
}

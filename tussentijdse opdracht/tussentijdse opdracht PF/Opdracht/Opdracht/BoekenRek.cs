﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht
{
    public class BoekenRek : IVoorwerpen
    {
        public decimal Winst
        {
            get
            {
                return Aankoopprijs * 2;
            }
        }

        public void GegevensTonen()
        {
            Console.WriteLine($"hoogte:{Hoogte}");
            Console.WriteLine($"breedte: {Breedte}");
            Console.WriteLine($"aankoopprijs: {Aankoopprijs}");
            Console.WriteLine($"Winst: {Winst}");
        }
        private decimal hoogteValue;

        public decimal Hoogte
        {
            get { return hoogteValue; }
            set
            {
                if (value>0m)
                {
                    hoogteValue = value;
                }
                 }
        }

        private decimal breedteValue;

        public decimal Breedte
        {
            get { return breedteValue; }
            set
            {
                if (value > 0m)
                {
                    breedteValue = value;
                }
                 }
        }
        private decimal aankoopprijsValue;

        public decimal Aankoopprijs
        {
            get { return aankoopprijsValue; }
            set { aankoopprijsValue = value; }
        }

        public BoekenRek(decimal hoogte, decimal breedte, decimal aankoopprijs)
        {
            Hoogte = hoogte;
            Breedte = breedte;
            Aankoopprijs = aankoopprijs;
            
        }




    }
}

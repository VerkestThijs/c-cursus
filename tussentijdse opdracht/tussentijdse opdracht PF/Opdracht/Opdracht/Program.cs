﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht
{
    class Program
    {
        static void Main(string[] args)
        {
            IVoorwerpen[] voorwerpen = new IVoorwerpen[3];
            voorwerpen[0] = new BoekenRek(8m, 5m, 5);
            voorwerpen[1] = new Leesboek("het boekig boek", "boek de boekeschrijver",  5m, new Genre("fictie", new Genre.Doelgroep(25)), "over een boek");
            voorwerpen[2] = new Woordenboek("het woordenboek", "woordenboekschrijver", 10m, new Genre("woordenboek", new Genre.Doelgroep(16)), "nederlands");
            decimal totalewinst = 0m;
            foreach (IVoorwerpen voorwerp in voorwerpen)
            {
                voorwerp.GegevensTonen();
                Console.WriteLine();
                totalewinst += voorwerp.Winst;
                
            }
            Console.WriteLine(totalewinst);
            
        }
    }
}

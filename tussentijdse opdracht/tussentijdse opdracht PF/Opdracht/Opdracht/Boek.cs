﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht
{
    public abstract class Boek : IVoorwerpen
    {
        public abstract decimal Winst {
            get;
                
            
        }

        public virtual void GegevensTonen()
        {
            Console.WriteLine($"titel { Titel}");
            Console.WriteLine($"auteur {Auteur}");
            Console.WriteLine($"eigenaar: {Eigenaar}");
            Console.WriteLine($"aankoopprijs: {Aankoopprijs}");
            Console.WriteLine($"Genre: {Genre}");
        }

        private string titelValue;

        public string Titel
        {
            get { return titelValue; }
            set { titelValue = value; }
        }

        private string auteurValue;

        public string Auteur
        {
            get { return auteurValue; }
            set { auteurValue = value; }
        }

        public static string Eigenaar = "vdab";

        private decimal aankoopprijsValue;

        public decimal Aankoopprijs
        {
            get { return aankoopprijsValue; }
            set { aankoopprijsValue = value; }
        }

        private Genre genrevalue;

        public Genre Genre
        {
            get { return genrevalue; }
            set { genrevalue = value; }
        }



        public Boek(string titel, string auteur, decimal aankoopprijs,Genre genre)
        {
            Titel = titel;
            Auteur = auteur;
            Aankoopprijs = aankoopprijs;
            Genre = genre;

        }




    }
}

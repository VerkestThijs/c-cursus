﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht
{
    public class Woordenboek : Boek
    {
        public override decimal Winst
        {
            get
            {
                return Aankoopprijs * 1.75m;
            }
        }

        private string taalValue;

        public string Taal
        {
            get { return taalValue; }
            set { taalValue = value; }
        }

        public override void GegevensTonen()
        {
            base.GegevensTonen();
            Console.WriteLine($"winst: {Winst}");
            Console.WriteLine($"taal: {Taal}");
        }

        public Woordenboek(string titel, string auteur, decimal aankoopprijs, Genre genre, string taal ): base(titel,auteur,aankoopprijs,genre)
        {
            
            Taal = taal;
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opdracht
{
   public  class Genre
    {
        private string naamValue;

        public string Naam
        {
            get { return naamValue; }
            set { naamValue = value; }
        }

        public Doelgroep Groep { get; set; }



        public class Doelgroep
        {
            private int leeftijdValue;

            public int Leeftijd
            {
                get { return leeftijdValue; }
                set { leeftijdValue = value; }
            }

            public string Categorie
            {
                get {if(Leeftijd<18)
                    {
                        return  "jeugd";
                        
                    }
                    else
                    {
                        return "volwassen";
                    }
                    

                }
                
            }
            public Doelgroep(int leeftijd)
            {
                Leeftijd = leeftijd;
            }
            public override string ToString()
            {
                return $"{Categorie}({Leeftijd})";
            }



        }
        

        public Genre(string naam,Doelgroep groep )
        {
            this.Naam = naam;
            Groep = groep;
            
            
        }
        public override string ToString()
        {
            return string.Format($"{Naam} {Groep}");
        }

    }
}

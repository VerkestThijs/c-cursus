﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPFOefenmap
{
    class Program
    {
        const string alfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        static void Main(string[] args)
        {
            /*
            const int GemLichTempCelcius = 37;
            int conversie = (GemLichTempCelcius * (9 / 5) + 32);
            Console.WriteLine("temp in farenheit bedraagt "+conversie + " graden");
            
            

            int geheleSconden = 5000;
            int uren = geheleSconden / 3600;
            int minuten = geheleSconden % 3600 / 60;
            int seconden = geheleSconden % 3600 % 60 % 60;
            Console.WriteLine($"uren : {uren} minuten : {minuten} seconden: {seconden} ");


            

            const int munt = 200;
            int prijs = 90;
            int wisselgeld = munt - prijs;

            int euroW = 100;
            int vijftigW = 50;
            int twintigW = 20;
            int tienW = 10;
            int vijfW = 5;
            int tweeW = 2;
            int eenW = 1;

            int euros = wisselgeld / euroW;
            int vijftigcent = wisselgeld % euroW / vijftigW;
            int twintigcent = wisselgeld % euroW % vijftigW / twintigW;
            int tiencent = wisselgeld % euroW % vijftigW % twintigW / tienW;
            int vijfc = wisselgeld % euroW % vijftigW % twintigW % tienW / vijfW;
            int tweec = wisselgeld % euroW % vijftigW % twintigW % tienW % vijfW / tweeW;
            int eenc = wisselgeld % euroW % vijftigW % twintigW % tienW % vijfW % tweeW / eenW;
     

            Console.WriteLine($"{ euros} {vijftigcent} {twintigcent} {tiencent} {vijfc} {tweec} {eenc}");

            */ /*
            Console.WriteLine("geef je aankoopbedrag in");
            decimal aankoopbedrag = int.Parse(Console.ReadLine());

            if (aankoopbedrag <= 25)
            {
                Console.WriteLine("de korting bedraagt 1%");
                decimal korting = (aankoopbedrag / 100 )* 1;
                decimal uitinedelijkGetal = (aankoopbedrag - korting);
                Console.WriteLine($"de korting komt neer op {uitinedelijkGetal}");
            }
            else if (aankoopbedrag > 25  && aankoopbedrag< 50)
            {
                Console.WriteLine("je korting bedraagt 2%");
                decimal korting = (aankoopbedrag / 100) * 1;
                decimal uitinedelijkGetal = (aankoopbedrag - korting);
                Console.WriteLine($"de korting komt neer op {uitinedelijkGetal}");
            }
            else if (aankoopbedrag <= 100)
            {
                Console.WriteLine("je korting bedraagt 3%");
                decimal korting = (aankoopbedrag / 100) * 1;
                decimal uitinedelijkGetal = (aankoopbedrag - korting);
                Console.WriteLine($"de korting komt neer op {uitinedelijkGetal}");
            }
            else
            {
                Console.WriteLine("je korting bedraagt 5%");
                decimal korting = (aankoopbedrag / 100) * 1;
                decimal uitinedelijkGetal = (aankoopbedrag - korting);
                Console.WriteLine($"de korting komt neer op {uitinedelijkGetal}");
            }


            */  /*
            Console.WriteLine("heef een jaartal in");
            int jaar = int.Parse(Console.ReadLine());

            if ((jaar%4 ==0 && jaar%100 != 0) || (jaar%400 ==0))
            {
                Console.WriteLine($" jaar {jaar} is een schrikkeljaar");
            }
            else
            {
                Console.WriteLine($"jaar {jaar} is geen schrikkeljaar");
            }

            */



            /*
            Console.WriteLine("geef een datum in");
            const string boodschapWeekdag = "we wensen u een prettige werkdag";
            const string boodschapWeekend = "we wensen u ee fijn weekend!";
            const string openingsurenWeekdag = "9:00 tot 12:00 en 13:00 tot 18:00";
            const string openingsurenWaterdag = "10:00 tot 12:00";
            const string openingsurenZondag = "gesloten";

            Console.Write("datum?");
            DateTime datum = DateTime.Parse(Console.ReadLine());
            StringBuilder boodschap = new StringBuilder("openingsuren");

            switch (datum.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    boodschap.Clear();
                    boodschap.Append(openingsurenZondag + Environment.NewLine + boodschapWeekend);
                    break;
                case DayOfWeek.Monday:
                
                case DayOfWeek.Tuesday:
                    
                case DayOfWeek.Wednesday:
                    
                case DayOfWeek.Thursday:
                    
                case DayOfWeek.Friday:
                    boodschap.Append(openingsurenWeekdag);
                    boodschap.AppendLine();
                    boodschap.Append(boodschapWeekdag);
                    break;
                case DayOfWeek.Saturday:
                    boodschap.Append(openingsurenWaterdag + Environment.NewLine + boodschapWeekend);
                    break;
                default:
                    break;
            }
            Console.WriteLine(boodschap.ToString());

            */
            /*
            string dag = Console.ReadLine();

            switch (dag)
            {
                case "maandag":
                case "dinsdag":
                case "woensdag":
                case "donderdag":
                case "vrijdag ":
                    Console.WriteLine("de openingsuren zijn van 9:00 tot 12:00 en van 13:00 tot 18:00");
                    Console.WriteLine("we wensen u een prettige werkdag");
                    break;
                case "zaterdag ":
                    Console.WriteLine("de openingsuren zijn van 10:00 tot 12:00");
                    Console.WriteLine("we wensen u een fijn weekend");
                    break;

                case "zondag ":
                    Console.WriteLine("we zijn vandaag gesloten; we wensen u een fijn weekend");
                    break;

                default:
                    Console.WriteLine("u hebt geen dag ingegeven");
                    break;
            }
            */

            /*

            int eenGetal;

            do
            {
                Console.WriteLine("geef een eerste postief geheel getal  of -1 (=stoppen) in :");
                eenGetal = Convert.ToInt32(Console.ReadLine());
            }
            while (eenGetal != -1 && eenGetal < 0);

            int kleinste = eenGetal, grootste = eenGetal, totaal = 0, aantal = 0;


            while (eenGetal != -1)
            {
                if (eenGetal >= 0)
                {
                    if (eenGetal < kleinste)
                    {
                        kleinste = eenGetal;
                    }
                    if (eenGetal > grootste)
                    {
                        grootste = eenGetal;
                    }

                    totaal += eenGetal;
                    aantal++;
                }

                else
                {
                    Console.WriteLine("enkel positieve getallen zijn toegelqten");
                    Console.WriteLine("eindigen met -1");
                }


                Console.WriteLine("geef een volgende positief geheel getql in  (-1 = stoppen)");
                eenGetal = Convert.ToInt32(Console.ReadLine());
            }

            if (aantal > 0)
            {
                Console.WriteLine("het kleinste getal : {0}" , kleinste);
                Console.WriteLine("het grootste getal : {0}", grootste);
                Console.WriteLine("het gemiddelde {0}", (double)totaal/aantal);

            }
            else
            {
                Console.WriteLine(" er werden geen getallen ingevoerd");
            }
           */

            /*
            int delers = 0;

            Console.WriteLine("voer een pos geheel getal in groter dan nul :");
            int getal = Convert.ToInt32(Console.ReadLine());
            if (getal > 0)
            {
                for (int teller = 2; teller <= getal -1; teller++)
                {
                    if (getal % teller == 0)
                    {
                        Console.WriteLine("getal is deelbaar door {0} ", teller);
                        delers++;
                    }
                    
                }
                if (delers > 0)
                {
                    Console.WriteLine(" het getal is geen priemgetal");
                }
                else
                {
                    Console.WriteLine("getal is een priemgetal.");
                    }
            }
            else
            {
                Console.WriteLine("verkeerde invoer");
            }

            */



            /*

            string ibanRekeningNr, controleNr, belgischRekeningNr, ibanRekeningNrControle, controleGetal;


            ulong rest97;


            belgischRekeningNr = "739-0102134-91";
            ibanRekeningNrControle = belgischRekeningNr.Replace("-", "") + "BE00";

            controleNr = VervangLetters(ibanRekeningNrControle);
            rest97 = ulong.Parse(controleNr) % 97ul;
            controleGetal = (98 - rest97).ToString();

            ibanRekeningNr = "BE" + (controleGetal.Length == 2 ? controleGetal : "0" + controleGetal) + ibanRekeningNrControle.Substring(0, ibanRekeningNrControle.Length - 4);

            ibanRekeningNr = ibanRekeningNr.Insert(12, " ").Insert(8, " ").Insert(4, " ");

            Console.WriteLine("rekeningnummer : {0} qls IBAN rekeningnummer : {1}", belgischRekeningNr,ibanRekeningNr);


            */

            /*
            const int aantalMaanden = 12;
            double[] temperaturen = new double[aantalMaanden];
            int maand;
            for (maand = 0 ; maand <= aantalMaanden; maand++)
            {
                Console.WriteLine("tik de temperatuur voor maand {0}:", maand);
                temperaturen[maand-1] = double.Parse(Console.ReadLine());
            }
            Console.WriteLine("je tikte de volgende temp");
            for (maand = 1;  maand<= aantalMaanden; maand ++)
            {
                Console.WriteLine("maand {0} : {1} ", maand, temperaturen[maand-1]);
            }*/
            /*
            const int aantalMaanden =12  ;
            int[] dagenPerMaand = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            for (int maand = 0; maand < dagenPerMaand.Length; maand++)
            {
                Console.WriteLine(dagenPerMaand[maand]); 
            }


            */
            /*
            Console.WriteLine("aantal ankrekeningen:");
            int aantalRekeningen = int.Parse(Console.ReadLine());
            ulong[] rekeningNrs = new ulong[aantalRekeningen];
            for (int teller = 1; teller <= aantalRekeningen; teller++)
            {
                Console.WriteLine(" nummer vand {0} rekening: ", teller);
                rekeningNrs[teller - 1] = ulong.Parse(Console.ReadLine());
            }
            Console.WriteLine("dit zijn de nummers van je rekeningen:");
            foreach (ulong rekeningNr in rekeningNrs)
            {
                Console.WriteLine(rekeningNr);
            }
            */
            /*
            string[] namen = { "asterix", "Obelix", "idefix" };
            Console.WriteLine("beginsituqtie");
            foreach (string naam in namen)
            {
                Console.WriteLine(naam);
            }
            Array.Sort(namen);
            Console.WriteLine("na sort:");
            foreach (string naam in namen)
            {
                Console.WriteLine(naam);

            }
            Array.Reverse(namen);
            Console.WriteLine("na reverse");
            foreach (string  naam in namen)
            {
                Console.WriteLine(naam);
            }
            string[] kopie = new string[namen.Length];
            Array.Copy(namen, kopie, namen.Length);
            foreach (string naam in kopie)
            {
                Console.WriteLine(naam);
            }
            Console.WriteLine("1° Idefix" + Array.IndexOf(namen,"idefix"));
            */


            char[] sleutel = { 'Q', 'S', 'P', 'A', 'T','V','X','B','C','R','J','Y', 'E', 'D', 'U', 'O', 'H', 'Z', 'G', 'I', 'F', 'L',
                'N', 'W','K','M' };

            Console.WriteLine("voer je tekst in :");
            string tekst = Console.ReadLine().ToUpper();
            string gecodeerd = string.Empty;
            for (int teller = 0; teller < tekst.Length; teller++)
            {
                if (tekst[teller] >= 'A' && tekst[teller] <= 'Z')
                {
                    gecodeerd += sleutel[(int)tekst[teller] - (int)'A'];
                }

                else
                {
                    gecodeerd += tekst[teller];
                }

                Console.WriteLine("in code: {0}", gecodeerd);
            }






















            Console.ReadLine();
        }


        /*
        private static string VervangLetters(string nummer)
        {
            char teken;
            string nr = string.Empty;
            for (int teller = 0; teller < nummer.Length; teller++)
            {
                teken = nummer[teller];
                if (teken >= 'A' && teken <= 'Z')
                {
                    nr += alfabet.IndexOf(teken) + 10;
                }
                else
                {
                    nr += teken;
                }
            }
            return nr;
        }*/
    }
}
